/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package util;

/**
 *
 * @author Usuario
 */
public interface Lista<T>
{
    public int adicionar(T o);
    
    public T remover(int p);
    
    public T remover(Object o);
    
    public T contem(Object o);
    
    public int size();
    
}
