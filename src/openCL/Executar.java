/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package openCL;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import org.jocl.CL;
import static org.jocl.CL.CL_CONTEXT_PLATFORM;
import static org.jocl.CL.CL_MEM_COPY_HOST_PTR;
import static org.jocl.CL.CL_MEM_READ_ONLY;
import static org.jocl.CL.CL_MEM_READ_WRITE;
import static org.jocl.CL.CL_TRUE;
import static org.jocl.CL.clBuildProgram;
import static org.jocl.CL.clCreateBuffer;
import static org.jocl.CL.clCreateCommandQueue;
import static org.jocl.CL.clCreateContext;
import static org.jocl.CL.clCreateKernel;
import static org.jocl.CL.clCreateProgramWithSource;
import static org.jocl.CL.clEnqueueNDRangeKernel;
import static org.jocl.CL.clEnqueueReadBuffer;
import static org.jocl.CL.clReleaseCommandQueue;
import static org.jocl.CL.clReleaseContext;
import static org.jocl.CL.clReleaseKernel;
import static org.jocl.CL.clReleaseMemObject;
import static org.jocl.CL.clReleaseProgram;
import static org.jocl.CL.clSetKernelArg;
import org.jocl.Pointer;
import org.jocl.Sizeof;
import org.jocl.cl_command_queue;
import org.jocl.cl_context;
import org.jocl.cl_context_properties;
import org.jocl.cl_device_id;
import org.jocl.cl_kernel;
import org.jocl.cl_mem;
import org.jocl.cl_program;

/**
 *
 * @author alexo
 */
public class Executar {

    private BufferedImage antes;
    private BufferedImage depois;
    private BufferedImage resultado;
    private Device device;
    private String codigo;
    private double precisao = 0;

    public Executar() {
        try {
            File f = new File("src/openCL/arquivo1.c");
            byte[] data = new byte[(int) f.length()];
            FileInputStream in = new FileInputStream(f);
            in.read(data);
            in.close();
            codigo = new String(data);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public BufferedImage getAntes() {
        return antes;
    }

    public void setAntes(BufferedImage antes) {
        this.antes = antes;
    }

    public BufferedImage getDepois() {
        return depois;
    }

    public void setDepois(BufferedImage depois) {
        this.antes = this.depois;
        this.depois = depois;
        if (depois != null) {
            this.resultado = new BufferedImage(depois.getWidth(), depois.getHeight(), 6);
        }
    }

    public Device getDevice() {
        return device;
    }

    public void setDevice(Device device) {
        this.device = device;
    }

    private int[] toArray(BufferedImage img) {

        int[] pixels = new int[img.getWidth() * img.getHeight() * 4];
        img.getRaster().getPixels(0, 0, img.getWidth(), img.getHeight(), pixels);
        return pixels;
    }

    public long executar() {
        if (antes == null || depois == null) {
            return -1;
        }
        CL.setExceptionsEnabled(true);
        cl_context_properties contextProperties = new cl_context_properties();
        contextProperties.addProperty(CL_CONTEXT_PLATFORM, device.getPlataforma().getCl_plataforma());
        cl_context context = clCreateContext(
                contextProperties, 1, new cl_device_id[]{
                    device.getCl_device()
                },
                null, null, null);
        cl_command_queue commandQueue
                = clCreateCommandQueue(context, device.getCl_device(), 0, null);

        int img1[] = toArray(antes);
        int img2[] = toArray(depois);
        int img3[] = new int[antes.getWidth() * antes.getHeight() * 4];

        Pointer srcA = Pointer.to(img1);
        Pointer srcB = Pointer.to(img2);
        Pointer dst = Pointer.to(img3);
        cl_mem memObjects[] = new cl_mem[4];
        memObjects[0] = clCreateBuffer(context,
                CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR,
                Sizeof.cl_int * img1.length, srcA, null);
        memObjects[1] = clCreateBuffer(context,
                CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR,
                Sizeof.cl_int * img2.length, srcB, null);
        memObjects[2] = clCreateBuffer(context,
                CL_MEM_READ_WRITE,
                Sizeof.cl_int * img3.length, null, null);
        memObjects[3] = clCreateBuffer(context,
                CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR,
                Sizeof.cl_double * img3.length, Pointer.to(new double[]{precisao}), null);

        cl_program program = clCreateProgramWithSource(context,
                1, new String[]{
                    codigo
                }, null, null);

        clBuildProgram(program, 0, null, null, null, null);
        cl_kernel kernel = clCreateKernel(program, "mapa", null);

        clSetKernelArg(kernel, 0,
                Sizeof.cl_mem, Pointer.to(memObjects[0]));
        clSetKernelArg(kernel, 1,
                Sizeof.cl_mem, Pointer.to(memObjects[1]));
        clSetKernelArg(kernel, 2,
                Sizeof.cl_mem, Pointer.to(memObjects[2]));
        clSetKernelArg(kernel, 3,
                Sizeof.cl_mem, Pointer.to(memObjects[3]));

        long global_work_size[] = new long[]{
            antes.getWidth() * antes.getHeight(), 1, 1
        };
        long local_work_size[] = new long[]{
            1, 1, 1
        };

        // Execute the kernel
        long ini = System.nanoTime();
        clEnqueueNDRangeKernel(commandQueue, kernel, 3, null,
                global_work_size, local_work_size, 0, null, null);

        // Read the output data
        clEnqueueReadBuffer(commandQueue, memObjects[2], CL_TRUE, 0,
                img3.length * Sizeof.cl_int, dst, 0, null, null);
        long fin = System.nanoTime();
        // Release kernel, program, and memory objects
//        for (int i = 0; i < img1.length; i++) {
//            System.out.print("[" + img1[i] + "]");
//        }
//        System.out.println("");
//        for (int i = 0; i < img3.length; i++) {
//            System.out.print("[" + img3[i] + "]");
//        }
//        System.out.println("");
        synchronized (this) {
            resultado.getRaster().setPixels(0, 0, resultado.getWidth(), resultado.getHeight(), img3);
        }
        clReleaseMemObject(memObjects[0]);
        clReleaseMemObject(memObjects[1]);
        clReleaseMemObject(memObjects[2]);
        clReleaseMemObject(memObjects[3]);
        clReleaseKernel(kernel);
        clReleaseProgram(program);
        clReleaseCommandQueue(commandQueue);
        clReleaseContext(context);
        return fin - ini;
    }

    public static void main(String[] args) {
        Device device = Device.todos()[0];
        System.out.println(Device.todos()[2].getNome());
        BufferedImage i1 = new BufferedImage(5, 5, 6);
        int p = 0;
        for (int i = 0; i < 5; i++) {
            for (int j = 0; j < 5; j++) {
                i1.setRGB(i, j, p++);
            }
        }
        BufferedImage i2 = new BufferedImage(5, 5, 6);
        Executar ex = new Executar();
        ex.antes = i1;
        ex.depois = i2;
        ex.device = device;

        System.out.println(ex.executar());
    }

    public synchronized BufferedImage getResultado() {
        return resultado;
    }

    public double getPrecisao() {
        return precisao;
    }

    public void setPrecisao(double precisao) {
        this.precisao = precisao;
    }
    
    
}

