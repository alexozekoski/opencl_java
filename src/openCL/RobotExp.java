package openCL;

import java.awt.AWTException;
import java.awt.Desktop;
import java.awt.Rectangle;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.URI;

import javax.imageio.ImageIO;

public class RobotExp {

    public static void main(String[] args) {

        try {
            URI link = new URI("https://www.youtube.com/watch?v=pAgnJDJN4VA&ab_channel=acdcVEVO");
            Desktop.getDesktop().browse(link);
        } catch (Exception erro) {
            System.out.println(erro);
        }

        try {

            Robot robot = new Robot();
            robot.delay(3000);
            robot.keyPress(KeyEvent.VK_F);
            robot.delay(2000);
            robot.keyPress(KeyEvent.VK_TAB);
            robot.keyPress(KeyEvent.VK_TAB);
            robot.keyPress(KeyEvent.VK_ENTER);
            robot.delay(5000);
            // Captura o screen shot da área definida pelo retângulo
            BufferedImage bi = robot.createScreenCapture(new Rectangle(1366, 808));
            //ImageIO.write(bi2, "jpg", new File("C:\\Users\\Usuario\\Documents\\NetBeansProjects\\opencl_java-master\\capture\\photo2.jpg"));
            File file = new File("opencl_java-master\\capture\\photo.jpg");
            file.mkdirs();
            ImageIO.write(bi, "jpg", file);
            robot.delay(5000);
            BufferedImage bi2 = robot.createScreenCapture(new Rectangle(1366, 808));
            ImageIO.write(bi2, "jpg", new File("C:\\Users\\Usuario\\Documents\\NetBeansProjects\\opencl_java-master\\capture\\photo2.jpg"));

        } catch (AWTException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
