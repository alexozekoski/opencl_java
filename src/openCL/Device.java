/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package openCL;

import org.jocl.cl_device_id;
import org.jocl.cl_platform_id;

/**
 *
 * @author alexo
 */
public class Device {

    private String nome;
    private String vendor;
    private String versao;
    private String tipo;
    private int maxComputeUnits;
    private long[] maxWrokItemDimensao;
    private int maxWorkGroupSize;
    private long maxClockFrequencey;
    private int addressBits;
    private long maxMemAllocSize;
    private long globalMemSize;
    private String localMemType;
    private cl_device_id cl_device;
    private Plataforma plataforma;

    public static Device[] todos() {
        int max = 0;
        Plataforma[] plataformas = Plataforma.todas();
        for (Plataforma plataforma : plataformas) {
            max += plataforma.getDevices().length;
        }
        Device[] devices = new Device[max];
        max = 0;
        for (Plataforma plataforma : plataformas) {
            for (Device device : plataforma.getDevices()) {
                devices[max++] = device;
            }
        }
        return devices;
    }

    public static Device[] GPU() {
        Device[] devices = todos();
        int cont = 0;
        for (Device device : devices) {
            if (device.tipo == "GPU") {
                cont++;
            }
        }
        Device[] gpus = new Device[cont];
        cont = 0;
        for (Device device : devices) {
            if (device.tipo == "GPU") {
                gpus[cont++] = device;
            }
        }
        return gpus;
    }

    public Device(String nome, String vendor, String versao, String tipo, int maxComputeUnits, long[] maxWrokItemDimensao, int maxWorkGroupSize, long maxClockFrequencey, int addressBits, long maxMemAllocSize, long globalMemSize, String localMemType, cl_device_id cl_device) {
        this.nome = nome;
        this.vendor = vendor;
        this.versao = versao;
        this.tipo = tipo;
        this.maxComputeUnits = maxComputeUnits;
        this.maxWrokItemDimensao = maxWrokItemDimensao;
        this.maxWorkGroupSize = maxWorkGroupSize;
        this.maxClockFrequencey = maxClockFrequencey;
        this.addressBits = addressBits;
        this.maxMemAllocSize = maxMemAllocSize;
        this.globalMemSize = globalMemSize;
        this.localMemType = localMemType;
        this.cl_device = cl_device;
    }

    public String getNome() {
        return nome;
    }

    public String getVendor() {
        return vendor;
    }

    public String getVersao() {
        return versao;
    }

    public String getTipo() {
        return tipo;
    }

    public int getMaxComputeUnits() {
        return maxComputeUnits;
    }

    public long[] getMaxWrokItemDimensao() {
        return maxWrokItemDimensao;
    }

    public int getMaxWorkGroupSize() {
        return maxWorkGroupSize;
    }

    public long getMaxClockFrequencey() {
        return maxClockFrequencey;
    }

    public int getAddressBits() {
        return addressBits;
    }

    public long getMaxMemAllocSize() {
        return maxMemAllocSize;
    }

    public long getGlobalMemSize() {
        return globalMemSize;
    }

    public String getLocalMemType() {
        return localMemType;
    }

    public Plataforma getPlataforma() {
        return plataforma;
    }

    public void setPlataforma(Plataforma plataforma) {
        this.plataforma = plataforma;
    }

    public cl_device_id getCl_device() {
        return cl_device;
    }

}
