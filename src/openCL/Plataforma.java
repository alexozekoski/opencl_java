/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package openCL;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import static org.jocl.CL.CL_DEVICE_ADDRESS_BITS;
import static org.jocl.CL.CL_DEVICE_GLOBAL_MEM_SIZE;
import static org.jocl.CL.CL_DEVICE_LOCAL_MEM_TYPE;
import static org.jocl.CL.CL_DEVICE_MAX_CLOCK_FREQUENCY;
import static org.jocl.CL.CL_DEVICE_MAX_COMPUTE_UNITS;
import static org.jocl.CL.CL_DEVICE_MAX_MEM_ALLOC_SIZE;
import static org.jocl.CL.CL_DEVICE_MAX_WORK_GROUP_SIZE;
import static org.jocl.CL.CL_DEVICE_MAX_WORK_ITEM_SIZES;
import static org.jocl.CL.CL_DEVICE_NAME;
import static org.jocl.CL.CL_DEVICE_TYPE;
import static org.jocl.CL.CL_DEVICE_TYPE_ACCELERATOR;
import static org.jocl.CL.CL_DEVICE_TYPE_ALL;
import static org.jocl.CL.CL_DEVICE_TYPE_CPU;
import static org.jocl.CL.CL_DEVICE_TYPE_DEFAULT;
import static org.jocl.CL.CL_DEVICE_TYPE_GPU;
import static org.jocl.CL.CL_DEVICE_VENDOR;
import static org.jocl.CL.CL_DRIVER_VERSION;
import static org.jocl.CL.CL_PLATFORM_NAME;
import static org.jocl.CL.clGetDeviceIDs;
import static org.jocl.CL.clGetDeviceInfo;
import static org.jocl.CL.clGetPlatformIDs;
import static org.jocl.CL.clGetPlatformInfo;
import org.jocl.Pointer;
import org.jocl.Sizeof;
import org.jocl.cl_device_id;
import org.jocl.cl_platform_id;

/**
 *
 * @author alexo
 */
public class Plataforma {

    private String nome;

    private Device[] devices = new Device[0];

    private cl_platform_id cl_plataforma;

    private Plataforma(String nome, Device[] devices, cl_platform_id cl_plataforma) {
        this.nome = nome;
        this.devices = devices;
        this.cl_plataforma = cl_plataforma;
    }

    public static Plataforma[] todas() {
        int numPlatforms[] = new int[1];
        clGetPlatformIDs(0, null, numPlatforms);
        cl_platform_id platforms[] = new cl_platform_id[numPlatforms[0]];
        clGetPlatformIDs(platforms.length, platforms, null);
        Plataforma[] plataformas = new Plataforma[platforms.length];
        for (int i = 0; i < platforms.length; i++) {
            String platformName = getString(platforms[i], CL_PLATFORM_NAME);
            int numDevices[] = new int[1];
            clGetDeviceIDs(platforms[i], CL_DEVICE_TYPE_ALL, 0, null, numDevices);
            cl_device_id devicesArray[] = new cl_device_id[numDevices[0]];
            clGetDeviceIDs(platforms[i], CL_DEVICE_TYPE_ALL, numDevices[0], devicesArray, null);
            Device[] devices = new Device[devicesArray.length];
            for (int d = 0; d < devicesArray.length; d++) {
                cl_device_id device = devicesArray[d];
                String nome = getString(device, CL_DEVICE_NAME);
                String vendor = getString(device, CL_DEVICE_VENDOR);
                String versao = getString(device, CL_DRIVER_VERSION);
                long deviceType = getLong(device, CL_DEVICE_TYPE);
                String tipo = "";
                if ((deviceType & CL_DEVICE_TYPE_CPU) != 0) {
                    tipo = "CPU";
                }
                if ((deviceType & CL_DEVICE_TYPE_GPU) != 0) {
                    tipo = "GPU";
                }
                if ((deviceType & CL_DEVICE_TYPE_ACCELERATOR) != 0) {
                    tipo = "ACCELERATOR";
                }
                if ((deviceType & CL_DEVICE_TYPE_DEFAULT) != 0) {
                    tipo = "DEFAULT";
                }
                int maxComputeUnits = getInt(device, CL_DEVICE_MAX_COMPUTE_UNITS);
                long maxWorkItemSizes[] = getSizes(device, CL_DEVICE_MAX_WORK_ITEM_SIZES, 3);
                int maxWorkGroupSize = (int) getSize(device, CL_DEVICE_MAX_WORK_GROUP_SIZE);
                long maxClockFrequency = getLong(device, CL_DEVICE_MAX_CLOCK_FREQUENCY);
                int addressBits = getInt(device, CL_DEVICE_ADDRESS_BITS);
                long maxMemAllocSize = getLong(device, CL_DEVICE_MAX_MEM_ALLOC_SIZE);
                long globalMemSize = getLong(device, CL_DEVICE_GLOBAL_MEM_SIZE);
                String localMemType = (getInt(device, CL_DEVICE_LOCAL_MEM_TYPE) == 1 ? "LOCAL" : "GLOBAL");
                Device dev = new Device(nome, vendor, versao, tipo, maxComputeUnits, maxWorkItemSizes, maxWorkGroupSize, maxClockFrequency, addressBits, maxMemAllocSize, globalMemSize, localMemType, device);
                devices[d] = dev;
            }
            Plataforma plataforma = new Plataforma(platformName, devices, platforms[i]);
            for (Device device : plataforma.devices) {
                device.setPlataforma(plataforma);
            }
            plataformas[i] = plataforma;
        }
        return plataformas;
    }

    private static int getInt(cl_device_id device, int paramName) {
        return getInts(device, paramName, 1)[0];
    }

    private static int[] getInts(cl_device_id device, int paramName, int numValues) {
        int values[] = new int[numValues];
        clGetDeviceInfo(device, paramName, Sizeof.cl_int * numValues, Pointer.to(values), null);
        return values;
    }

    private static long getLong(cl_device_id device, int paramName) {
        return getLongs(device, paramName, 1)[0];
    }

    private static long[] getLongs(cl_device_id device, int paramName, int numValues) {
        long values[] = new long[numValues];
        clGetDeviceInfo(device, paramName, Sizeof.cl_long * numValues, Pointer.to(values), null);
        return values;
    }

    private static String getString(cl_device_id device, int paramName) {
        long size[] = new long[1];
        clGetDeviceInfo(device, paramName, 0, null, size);
        byte buffer[] = new byte[(int) size[0]];
        clGetDeviceInfo(device, paramName, buffer.length, Pointer.to(buffer), null);
        return new String(buffer, 0, buffer.length - 1);
    }

    private static String getString(cl_platform_id platform, int paramName) {
        long size[] = new long[1];
        clGetPlatformInfo(platform, paramName, 0, null, size);
        byte buffer[] = new byte[(int) size[0]];
        clGetPlatformInfo(platform, paramName, buffer.length, Pointer.to(buffer), null);
        return new String(buffer, 0, buffer.length - 1);
    }

    private static long getSize(cl_device_id device, int paramName) {
        return getSizes(device, paramName, 1)[0];
    }

    static long[] getSizes(cl_device_id device, int paramName, int numValues) {
        ByteBuffer buffer = ByteBuffer.allocate(
                numValues * Sizeof.size_t).order(ByteOrder.nativeOrder());
        clGetDeviceInfo(device, paramName, Sizeof.size_t * numValues,
                Pointer.to(buffer), null);
        long values[] = new long[numValues];
        if (Sizeof.size_t == 4) {
            for (int i = 0; i < numValues; i++) {
                values[i] = buffer.getInt(i * Sizeof.size_t);
            }
        } else {
            for (int i = 0; i < numValues; i++) {
                values[i] = buffer.getLong(i * Sizeof.size_t);
            }
        }
        return values;
    }

    public Device[] getDevices() {
        return devices;
    }

    public String getNome() {
        return nome;
    }

    public cl_platform_id getCl_plataforma() {
        return cl_plataforma;
    }

    
}
