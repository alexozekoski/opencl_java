/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package browser;

import com.github.kiulian.downloader.OnYoutubeDownloadListener;
import com.github.kiulian.downloader.YoutubeDownloader;
import com.github.kiulian.downloader.YoutubeException;
import com.github.kiulian.downloader.model.VideoDetails;
import com.github.kiulian.downloader.model.YoutubeVideo;
import com.github.kiulian.downloader.model.formats.VideoFormat;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import java.awt.Component;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;
import javax.imageio.ImageIO;
import javax.swing.JFrame;
import openCL.Device;
import openCL.Executar;

/**
 *
 * @author alexo
 */
public class Interface implements Runnable {

    Navegador navegador;

    List<VideoFormat> videosFormat;
    String videoId;
    String tumb;
    YoutubeVideo video;
    File pasta = new File("videos");
    Executar executar = new Executar();
    Frame frame;
    Thread exec = null;
    int device = 0;
    boolean rodando = false;
    JFrame tela;

    public Interface(Navegador navegador, Frame frame, JFrame tela) {
        this.navegador = navegador;
        this.frame = frame;
        this.tela = tela;
    }

    public void videos() {
        File[] lista = pasta.listFiles();
        String pastas = "[";
        if (lista != null && lista.length > 0) {
            for (File file : lista) {
                if (file.isDirectory()) {
                    if (pastas.equals("[")) {
                        pastas += "'" + file.getName() + "'";
                    } else {
                        pastas += ", '" + file.getName() + "'";
                    }
                }
            }

        }
        pastas += "]";
        navegador.executar("VUE.pastas = " + pastas);
    }

    public boolean resolucao(String id) {
        YoutubeDownloader downloader = new YoutubeDownloader();

        //downloader.addCipherFunctionPattern(2, "\\b([a-zA-Z0-9$]{2})\\s*=\\s*function\\(\\s*a\\s*\\)\\s*\\{\\s*a\\s*=\\s*a\\.split\\(\\s*\"\"\\s*\\)");
        downloader.setParserRequestProperty("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36");
        downloader.setParserRetryOnFailure(0);

        videoId = id;
        System.out.println(id);
        try {
            video = downloader.getVideo(videoId);
            VideoDetails details = video.details();
            String nome = details.title();
            // details.thumbnails().forEach(image -> System.out.println("Thumbnail: " + image));
            tumb = details.thumbnails().get(details.thumbnails().size() - 2).split("\\?")[0];
            videosFormat = video.videoFormats();

            String format = "{";
            for (int i = 0; i < videosFormat.size(); i++) {
                VideoFormat a = videosFormat.get(i);
                if (a.mimeType().contains("video/mp4; codecs=\"avc1")) {
                    if (format.equals("{")) {
                        format += i + ": '" + a.height() + "'";
                    } else {
                        format += ", " + i + ": '" + a.height() + "'";
                    }
                }
            }
            format += "}";
            String script = "VUE.tumb = '" + tumb + "'; VUE.formatos = " + format + "; VUE.nome = '" + nome.replace("'", "\\'") + "'";
            navegador.executar(script);
        } catch (YoutubeException ex) {
            ex.printStackTrace();
            return false;
        }
        return true;
    }

    public void deletar(String dir) {
        File file = new File(pasta, dir);
        if (file.exists()) {
            File[] files = file.listFiles();
            if (files != null) {
                for (File f : files) {
                    f.delete();
                }
            }
            file.delete();
        }
    }

    public void print(String s) {
        System.out.println(s);
    }

    public void print() {
        File file = new File("print");
        file.mkdir();
        file = new File(file, "" + System.currentTimeMillis() + ".png");
        BufferedImage img = new BufferedImage(navegador.getWidth(), navegador.getHeight(), BufferedImage.TYPE_INT_RGB);
        navegador.paint(img.getGraphics());
        try {
            ImageIO.write(img, "png", file);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    public void showFrames(boolean v) {
        frame.setVisible(v);
        if (!v) {
            rodando = false;
        }
    }

    public String gpus() {
        JsonArray lista = new JsonArray();

        for (Device device : Device.GPU()) {
            JsonObject ob = new JsonObject();
            ob.addProperty("nome", device.getNome());
            ob.addProperty("versao", device.getVersao());
            ob.addProperty("clock", device.getMaxClockFrequencey());
            ob.addProperty("uni", device.getMaxComputeUnits());
            lista.add(ob);
        }
        return lista.toString();
    }

    public void executar(int device) {
        this.device = device;
        rodando = true;
        exec = new Thread(this);
        exec.start();
    }

    public void pararExecutar() {
        rodando = false;
    }

    public void download(int formato) {
        File pasta = new File(this.pasta, videoId);
        pasta.mkdirs();
        URL url;
        try {
            url = new URL(tumb);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");
            InputStream in = conn.getInputStream();
            byte[] buffer = new byte[50000];
            int p;
            FileOutputStream saida = new FileOutputStream(new File(pasta, "tumb.jpg"));
            do {
                p = in.read(buffer);
                if (p != -1) {
                    saida.write(buffer, 0, p);
                }
            } while (p != -1);
            saida.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        File v = new File(pasta, "/video.mp4");
        if (v.exists()) {
            v.delete();
        }
        try {
            video.downloadAsync(videosFormat.get(formato), pasta, "video", new OnYoutubeDownloadListener() {
                @Override
                public void onDownloading(int i) {
                    navegador.executar("VUE.progressoDownload(" + i + ")");
                }

                @Override
                public void onFinished(File file) {
                    navegador.executar("VUE.concluidoDownload()");
                }

                @Override
                public void onError(Throwable thrwbl) {
                    navegador.executar("VUE.erroDownload()");
                }
            });

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
    
    public void precisao(double v)
    {
        executar.setPrecisao(v);
    }

    @Override
    public void run() {
        Device device = Device.GPU()[this.device];
        executar.setDevice(device);
        executar.setDepois(null);
        executar.setAntes(null);
        long ti, tf, temp = 0;

        while (rodando) {
            try {
                ti = System.currentTimeMillis();
                BufferedImage img = new BufferedImage(tela.getContentPane().getWidth(), tela.getContentPane().getHeight(), 6);
                BufferedImage video = img.getSubimage(0, Math.round(tela.getContentPane().getHeight() / 2.0f), Math.round(tela.getContentPane().getWidth() / 2.0f), tela.getContentPane().getHeight() / 2);
                navegador.paint(img.getGraphics());
                executar.setDepois(video);
                long tempo = executar.executar();

                if (tempo != -1) {
                    frame.setFundo(executar.getResultado());
                }
                tf = System.currentTimeMillis();
                temp += tf - ti;
                if (temp > 100) {
                    navegador.executar("VUE.addTempo(" + tempo / 1000000.0 + ")");
                    temp = 0;
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
        frame.setFundo(null);
    }
}
