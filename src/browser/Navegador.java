/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package browser;

import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.util.Map;
import java.util.TreeMap;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.concurrent.Worker;
import javafx.concurrent.Worker.State;
import javafx.embed.swing.JFXPanel;
import javafx.scene.Scene;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import netscape.javascript.JSObject;

/**
 *
 * @author Usuario
 */
public class Navegador extends JFXPanel {

    private WebEngine webEngine;
    private boolean executado = false;
    private NavegadorAcao acao = null;
    private Map<String, Object> objetos = new TreeMap();
    BufferedImage fundo = null;

    public Navegador() {
        this(null);
    }

    public void addObject(String nome, Object object) {
        objetos.put(nome, object);
    }

    public Navegador(NavegadorAcao acaoEvento) {
        this.acao = acaoEvento;
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                WebView browser = new WebView();

                String user = "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.129 Safari/537.36";
                // user = "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.129 Safari/537.36";
                browser.setUserData(user);
                webEngine = browser.getEngine();
                webEngine.getLoadWorker().stateProperty().addListener(new ChangeListener<State>() {
                    @Override
                    public void changed(ObservableValue<? extends State> ov, State t, State t1) {
                        if (t1 == Worker.State.SUCCEEDED) {
                            //webEngine.executeScript("setTimeout(() => {document.getElementById('ytp-id-17').innerHTML = ''},5000)");
                            setVisible(true);
                            JSObject window = (JSObject) webEngine.executeScript("window");
                            for (String nome : objetos.keySet()) {
                                window.setMember(nome, objetos.get(nome));
                            }
                        }
                    }
                });

                webEngine.locationProperty().addListener(new ChangeListener<String>() {
                    @Override
                    public void changed(ObservableValue<? extends String> ov, String t, String t1) {
                        if (acao != null) {
                            acao.URLMudou(t1);
                        }
                    }
                });
                Scene scene = new Scene(browser, 1920, 1080);

                setScene(scene);
                notificar();
            }
        });
        esperar();
    }

    private synchronized void esperar() {
        if (!executado) {
            try {
                wait();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
        executado = false;
    }

    private synchronized void notificar() {
        executado = true;
        notifyAll();
    }

    public void executar(String script) {
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                webEngine.executeScript(script);
            }
        });
    }

    public void lerUrl(String url) {
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                webEngine.load(url);
                notificar();
            }
        });
        esperar();
    }

    public void refresh() {
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                webEngine.reload();
                notificar();
            }
        });
        esperar();
    }

    public void lerString(String conteudo) {
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                webEngine.loadContent(conteudo);
                notificar();
            }
        });
        esperar();
    }

    public void addF5Refresh() {
        addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                if (evt.getKeyCode() == 116) {
                    refresh();
                }
            }
        });
    }

    public void definirAcao(NavegadorAcao acao) {
        this.acao = acao;
    }

    public NavegadorAcao obterAcao() {
        return this.acao;
    }

    @Override
    public void paint(Graphics g) {
        super.paint(g);
        
    }
    
    public void pedir(Runnable r)
    {
        Platform.runLater(r);
    }

    @Override
    public void setBounds(int x, int y, int width, int height) {
        fundo = new BufferedImage(width, height, 6);
        super.setBounds(x, y, width, height);
    }

}
