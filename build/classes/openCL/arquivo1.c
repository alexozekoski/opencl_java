
__kernel void mapa(__global const int *img1, __global const int *img2, __global int *img3, __global const double *pre) {
    int p = get_global_id(0) * 4;
    int r1 = img1[p];
    int r2 = img2[p];

    int g1 = img1[p + 1];
    int g2 = img2[p + 1];

    int b1 = img1[p + 2];
    int b2 = img2[p + 2];

    if (dif(r1, r2, pre) || dif(g1, g2, pre) || dif(b1, b2, pre)) {
        img3[p] = 255;
        img3[p + 1] = 0;
        img3[p + 2] = 0;
        img3[p + 3] = 255;
    } else {
        img3[p] = 0;
        img3[p + 1] = 0;
        img3[p + 2] = 255;
        img3[p + 3] = 255;
    }

}

int dif(int a, int b, const double *pre) {
    int d = a - b;
    if (d < 0) {
        d *= -1;
    }
    if (d > pre[0]) {
        return 1;
    }
    return 0;
}