
const VUE = new Vue({
    el: "#app",
    data: {
        url: '',
        formatos: {},
        nome: '',
        url_en: true,
        qual: 0,
        show_dowload: false,
        erro: false,
        sucesso: false,
        progresso: 0,
        show_progresso: false,
        pastas: [],
        video: null,
        print: false,
        pasta: '',
        baixar: true,
        apli: null,
        drivers: [],
        executar: false,
        drive: 0,
        grafico: null,
        dados: [],
        labels: [],
        precisao: 15
    },
    methods: {
        baixarVideo() {
            this.progresso = 0;
            this.show_dowload = false;
            this.url_en = false;
            this.erro = false;
            this.show_progresso = true;
            this.sucesso = false;
            JAVA.download(this.qual);
        },
        progressoDownload(val) {
            this.progresso = val;
        },
        concluidoDownload() {
            this.url_en = true;
            this.erro = false;
            this.show_dowload = true;
            this.show_progresso = false;
            this.sucesso = true;
            JAVA.videos();
        },
        erroDownload() {
            this.erro = true;
            this.url_en = true;
            this.show_dowload = false;
            this.show_progresso = false;
            this.deletar(this.pasta);
        },
        capturarPrint() {
            this.print = true;
            setTimeout(() => {
                JAVA.print();
                this.print = false;
            }, 1000);
        },
        deletar(dir) {
            JAVA.deletar(dir);
            JAVA.videos();
        },
        aplicar() {
            this.url_en = false;
            this.url = this.url.replace('https://www.youtube.com/watch?v=', '');
            this.pasta = this.url;
            this.sucesso = false;
            this.erro = false;
            setTimeout(() => {
                JAVA.resolucao(this.url);
                this.url_en = true;
            }, 100)
        },
        executarTerminar() {
            JAVA.pararExecutar();
            this.executar = false;
        },
        executarCancelar() {
            JAVA.pararExecutar();
            this.executar = false;
            this.$refs.videoShow.pause();
            this.$refs.videoShow.currentTime = 0;
            this.resetGrafico();
        },
        executarVideo() {
            this.$refs.videoShow.play();
            this.resetGrafico();
            JAVA.executar(this.drive);
            this.executar = true;
        },
        addTempo(t) {
            var a = this.$refs.videoShow.currentTime;
            var p = parseInt(a);
            if (p === undefined || a === undefined) {
                return;
            }
            var novo = this.labels[p] == null;

            if (novo) {
                this.labels[p] = a.toFixed(2);
                // if (this.labels.length > 10) {
                //     this.labels.pop();
                // }
                this.dados[p] = t;
                // if (this.dados.length > 10) {
                //     this.dados.pop();
                // }

            } else {
                //this.dados[p] += t;
            }

            if (novo) {
                this.grafico.data.labels = this.labels;
                this.grafico.data.datasets[0].data = this.dados;
                this.grafico.update();
            }

        },
        resetGrafico() {
            this.dados = [];
            this.labels = [];
            this.grafico.data.labels = this.labels;
            this.grafico.data.datasets[0].data = this.dados;
            this.grafico.update();
        },
        iniciarGrafico() {
            this.dados = [];
            this.labels = [];
            var ctx = document.getElementById('grafico-canvas').getContext('2d');
            var config = {
                type: 'line',
                data: {
                    labels: [],
                    datasets: [{
                        label: 'Tempo ms',
                        backgroundColor: '#8c03fc',
                        borderColor: '#8c03fc',
                        color: '#ffffff',
                        fill: false,
                        data: [
                        ],
                    }]
                },
                options: {
                    responsive: false,
                    legend: {
                        display: false
                    },
                    title: {
                        display: true,
                        text: 'Tempo de processamento em milissegundos (ms)'
                    },
                    scales: {
                        xAxes: [{
                            display: true,
                        }],
                        yAxes: [{
                            display: true,

                        }]
                    }
                }
            };
            this.grafico = new Chart(ctx, config);
        }
    },
    watch: {
        precisao(precisao) {
            JAVA.precisao(precisao);
        },
        formatos(form) {
            var keys = Object.keys(form);
            this.erro = false;
            if (keys.length > 0) {
                this.qual = keys[0];
                this.show_dowload = true;
            } else {
                this.show_dowload = false;
            }
        },
        apli(apli) {
            if (apli != null) {
                JAVA.showFrames(true);
                setTimeout(() => {
                    try {
                        this.iniciarGrafico();
                    } catch (e) {
                        JAVA.print(e);
                    }
                }, 100);
            } else {
                if (this.grafico) {
                    this.grafico.destroy();
                }
                JAVA.showFrames(false);
                this.executar = false;
            }
        }
    },
    mounted() {
        setTimeout(() => {
            JAVA.videos();
            this.drivers = JSON.parse(JAVA.gpus());
        }, 500);
    }
})



